module Spree
  SpreeFedexAddressValidator::Config = Spree::FedexAddressValidatorSettings.new
  SpreeFedexAddressValidator::Config.fedex_address_validator_key = ''
  SpreeFedexAddressValidator::Config.fedex_address_validator_password = ''
  SpreeFedexAddressValidator::Config.fedex_address_validator_account_number = ''
  SpreeFedexAddressValidator::Config.fedex_address_validator_meter = ''
  SpreeFedexAddressValidator::Config.fedex_address_validator_mode = ''
end

Spree::PermittedAttributes.checkout_attributes << :skip_fedex_validation
