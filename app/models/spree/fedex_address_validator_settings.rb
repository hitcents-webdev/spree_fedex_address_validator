module Spree
  class FedexAddressValidatorSettings < Preferences::Configuration
    preference :fedex_address_validator_key, :string
    preference :fedex_address_validator_password, :string
    preference :fedex_address_validator_account_number, :string
    preference :fedex_address_validator_meter, :string
    preference :fedex_address_validator_mode, :string
  end
end