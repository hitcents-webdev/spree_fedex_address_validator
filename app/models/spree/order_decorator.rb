module Spree
  Order.class_eval do

    attr_accessor :skip_fedex_validation

    validate :shipping_address_is_valid_via_fedex, if: :should_perform_fedex_validation

    private

      def shipping_address_is_valid_via_fedex
        changes = ship_address.fedex_response.changes

        if changes.include?("ZIP") || changes.include?("MODIFIED_TO_ACHIEVE_MATCH") || ship_address.fedex_response.confirmed == false
          errors.add(:base, Spree.t(:fedex_address_ambiguous))
        end
      end

      def should_perform_fedex_validation
        skip_fedex_validation == "0" && ship_address.is_us_50?
      end
  end
end
