module Spree
  Address.class_eval do

    @@us_50_states = %w[AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI WY]

    # adding US Territories since FedEx can validate those too
    @@us_50_states += %w[AS GU MP PR VI] 

    @fedex_response = nil
    @fedex_suggestions = nil

    def fedex_response
      @fedex_response ||= get_fedex_response
    end

    def fedex_suggestions
      suggestions = fedex_response.validation_result.parsed_response["AddressValidationReply"]["AddressResults"]["ProposedAddressDetails"]["Address"];

      if(suggestions["PostalCode"] != zipcode || fedex_response.changes.include?("MODIFIED_TO_ACHIEVE_MATCH"))
        suggestion = OpenStruct.new
        suggestion.street1 = suggestions["StreetLines"]
        suggestion.street2 = '' # ?
        suggestion.city = suggestions["City"]
        suggestion.zip = suggestions["PostalCode"]
        suggestion.state = suggestions["StateOrProvinceCode"]
        @fedex_suggestions = [suggestion]
      else
        @fedex_suggestions = []
      end
    end

    def is_us_50?
      # return true if it's one of the US 50 states. otherwise, UPS Address Validation won't cover it
      state.country.iso3 == "USA" && @@us_50_states.include?(state.abbr)
    end

    private
      def get_fedex_response
        response = Fedex::Address.validate_address({
          credentials: {
            key: SpreeFedexAddressValidator::Config.fedex_address_validator_key,
            password: SpreeFedexAddressValidator::Config.fedex_address_validator_password,
            account_number: SpreeFedexAddressValidator::Config.fedex_address_validator_account_number,
            meter: SpreeFedexAddressValidator::Config.fedex_address_validator_meter,
            mode: SpreeFedexAddressValidator::Config.fedex_address_validator_mode
          },
          address: {
            address: address1,
            city: city,
            state: state_text,
            postal_code: zipcode,
            country: country.try(:iso)
          }
        })

        # Fedex says NO_CHANGES even when it adds the additional 4 to a zip code
        suggestions = response.validation_result.parsed_response["AddressValidationReply"]["AddressResults"]["ProposedAddressDetails"]["Address"];

        if suggestions["PostalCode"] != zipcode
          # NOTE: I don't know if 'changes' can be both an array or a string,
          # but I saw a crash when it was an array and the code only expected it
          # to be a string, so I try to handle both scenarios
          response.changes += " ZIP" if response.is_a?(String)
          response.changes << "ZIP" if response.is_a?(Array)
        end

        self.address_type = response.business ? 'commercial' : 'residential'

        response
      end
  end
end
